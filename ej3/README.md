## Ejercicio 3

### Intro

Este problema es más bien conceptual, requiere que propongas una solución a un
problema especifico, ideando una solución integral al mismo. La presentación de
la solución puede ser un diagrama en bloques con un resumen explicado, una
presentación, una descripción en VHDL/Verilog, lo que te sea mas útil para
contarnos tu idea.

Se evaluará:

* Creatividad en la idea propuesta.
* Cobertura de especificación.
* Claridad en la explicación.
* Recursos utilizados.

### Descripción del problema

Se dispone de un System on Chip (SoC) que contiene:

* Una zona de lógica programable (PL)
* Un controlador de memoria DDR4
* 4 Interfaces AXI3 de tamaño de burst máximo 256 transacciones y 128 bits de
ancho máximo por palabra.
* Una CPU que puede acceder a la memoria DDR4 y la zona de la PL.

Todo esto se esquematiza en la siguiente figura.

```
                         SOC
+-----------------------------------------------------+
|                                                     |
|                                                     |      +-------------+
|                                                     |      |             |
|  +------------------+            +---------------+  |      |             |
|  |                  |   AXIx4    |               |  |      |             |
|  |                  <------------>               |  |      |             |
|  |                  |            |               |  |      |             |
|  |                  <------------>    MEMORY     |  |      |             |
|  |        PL        |            |               <--------->     DDR4    |
|  |                  <------------>  CONTROLLER   |  |      |             |
|  |                  |            |               |  |      |             |
|  |                  <------------>               |  |      |             |
|  |                  |            |               |  |      |             |
|  +--------^---------+            +-------^-------+  |      |             |
|           |                              |          |      |             |
|           |                              |          |      |             |
|  +--------+------------------------------+-------+  |      +-------------+
|  |                                               |  |
|  |                    CPU                        |  |
|  |                                               |  |
|  |                                               |  |
|  +-----------------------------------------------+  |
+-----------------------------------------------------+
```

Se requiere diseñar un core en la PL lo mas simple posible que permita medir el
ancho de banda hacia la DDR4.

Se debe explicar como es el método para hacer esta medición, en qué consiste, 
que recursos cree que necesitará y como se puede garantizar que lo medido es 
realmente el ancho de banda máximo de la memoria. A su vez indicar las 
limitaciones del método elegido.

____

# Resolución propuesta:
## Análisis:
En base a la especificación, se calcula un tamaño de archivo (en principio, de base) a los efectos de realizar la medición:
- Dados 128 bits de palabra con 256 transacciones, se puede realizar el siguiente cálculo:
> 128 bits=16 bytes
 
> 16 bytes x 256 transacciones = 4096 bytes
 
Con esto se concluye que con un archivo de 4kb podrá calcularse el ancho de banda hacia la memoria DDR4 en una iteración burst de 256 transacciones.
 
Además, dado que se dispone de 4 buses AXI de 32 bits, se podrá tomar de a 4 bytes por bus, completando así los 128 bits mencionados con anterioridad. Esto exigirá un proceso de separación del archivo.
 
Para esto se propone la siguiente estructura en PL:
 
[![Block_diagram](https://gitlab.com/ingenieria_am/test-hdl-satellogic/-/raw/master/ej3/ej3_block_diagram.png)](https://gitlab.com/ingenieria_am/test-hdl-satellogic/-/blob/master/ej3/ej3_block_diagram.png)
 
## Procedimiento a través de PL
Se plantea un sistema compuesto por dos contadores y lógica de "trozado" del archivo provisto por el CPU del tamaño mencionado con anterioridad. En funcionamiento consiste en:
 
- Al llegar el archivo, el mismo se divide en tramos de 32 bits, uno para cada bus AXI.
- Cuando los tramos estén disponibles, se habilitará el conteo de transacciones, el cual tendrá su señal TC (terminal count) como partícipe de la finalización del segundo contador.
- El contador de tiempo se activa también al comenzar la comunicación y finaliza cuando se termina la última transacción en todos los buses.
- El módulo del contador temporal deberá definirse en función a alguna referencia que no hace parte de este análisis.
- La señal TC del contador de tiempo dará cuenta de la obtención de la cantidad de ciclos de clk empleados en el proceso y entrega al CPU el valor de cuenta final. Conocida la frecuencia de clk, solo resta calcular el tiempo empleado como 
 
> time spent = count / f_clk
 
> band width = 1 / time spent
 
 
## Procedimiento a través de CPU
Bajo el mismo criterio, y siendo que el controlador de memoria es compartido, puede emplearse los recursos de HW de la CPU para realizar el conteo mediante su driver e interrupciones pertinentes. De este modo se corrobora con la misma operatoria el valor obtenido.
 
## Limitaciones
- Concretamente, lo que se mide es el ancho de banda del controlador. Deberá evaluarse la especificación del mismo para poder garantizar que no introduzca latencia. Si fuera el caso de no existir latencia, podría decirse que se conoce el ancho de banda de la memoria.
- Aún en la peor condición de latencia, como lo solicitado es estrictamente el tiempo de __acceso__ a memoria, el mismo sí está garantizado.
- Deberá tenerse especial cuidado en el diseño del "trozador" dado que el tick de inicio de cuenta, de administrarse mal, puede introducir un error apreciable en la cuenta.
- Si bien desde la CPU se puede realizar la operatoria, debe tenerse en cuenta los tiempos de atención de interrupción en juego para poder informarse el error cometido.
 
## Validación
Para tener noción del procedimiento y sus virtudes, se propone realizar un test bench del tipo __post place and route__ de manera que contemple todos los tiempos propios involucrados. Este test deberá contemplar el ancho de banda de la especificación del diseño, controlador y memoria, para variar luego en un entorno de +- 10%.
