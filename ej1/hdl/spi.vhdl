-- El la trama SPI debe responder a la siguiente waveform:

-- ```
--      +-------+                                               +---------+
--   CS         |                                               |
--              +-----------------------------------------------+

--                 +--+  +--+  +--+  +--+  +--+  +--+  +--+  +--+
-- SCLK            |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
--      +----------+  +--+  +--+  +--+  +--+  +--+  +--+  +--+  +---------+

--              +-----+-----+-----+-----+-----+-----+-----+-----+
-- MOSI +-------+ MSB |     |     |     |     |     |     | LSB +---------+
--              +-----+-----+-----+-----+-----+-----+-----+-----+

--              +-----+-----+-----+-----+-----+-----+-----+-----+
-- MISO +-------+ MSB |     |     |     |     |     |     | LSB +---------+
--              +-----+-----+-----+-----+-----+-----+-----+-----+

--                                                              +--+
--  INT                                                         |  |
--      +-------------------------------------------------------+  +-----+
--       ______   _____________________________________________   ___   ____
--             \ /                                             \ /   \ /
-- FSM    IDLE  |                    RUN_SCLK                   | INT | IDLE
--       ______/ \_____________________________________________/ \___/ \____

-- ```

--          +---------------------------------------------+
--          |   +-------------------------------------+   |
--          |   |  +-----------+                      |   |   +-----------+ 
--          |   |  |           |                      |   +-->|           |----> SCLK
-- AXI_CLK--+---|->|           |                      +------>|           |----> INT
-- AXI_RESETn---+->|  counter  |---->SCLK_count-------------->|    fsm    |--+-> CSn
-- +---CSn_signal->|           |---->data_count-------------->|           |  |
-- |               |           |                     WADDR--->|           |  |
-- |               +-----------+                              +-----------+  |
-- +-------------------------------------------------------------------------+

--               C_S_AXI_DATA_WIDTH        
--             +---------/------+    
--             |   +----------+ |                
--             +-->|          | |                _
--      AXI_CLK--->|          | |               | \
--   AXI_RESETn--->|   SIPO   |-+-->sipo_reg--->|1 \
--         S_IN--->|          |                 |   |--->P_OUT
--      S_load---> |          |          "0"--->|0 /
--                 +----------+                 |_/ 
--                                               ^                                              
--                                               |
--                                    P_read-----+

--               C_S_AXI_DATA_WIDTH        
--             +---------/------+    
--             |   +----------+ |                
--             +-->|          | |                
--      AXI_CLK--->|          | |               +-----+     
--   AXI_RESETn--->|          |-+-->piso_reg--->| MSB |--->S_OUT
--         P_IN--->|   PISO   |                 +-----+
--       P_load--->|          | 
--       S_read--->|          |          
--                 +----------+     
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use ieee.math_real.all;

entity spi is
generic (
    C_S_AXI_DATA_WIDTH : integer :=32;
    C_S_AXI_ADDR_WIDTH : integer :=4
);
  port (
        -- -- spi
            CSn_O         :out std_logic;            
            MOSI_O        :out std_logic;            
            MISO_I        :in std_logic;            
            SCLK_O        :out std_logic;            
            INT_O         :out std_logic;  
        -- AXI     
            WDATA_I       :in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            RDATA_O       :out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            RADDR_I       :in std_logic_vector(C_S_AXI_ADDR_WIDTH -1 downto 0);
            WADDR_I       :in std_logic_vector(C_S_AXI_ADDR_WIDTH -1 downto 0);
            WENA_I        :in std_logic;                           
            RENA_I        :in std_logic;
            AXI_CLK_I     :in std_logic;       
            AXI_RESETn_I  :in std_logic      
        );
end spi;

architecture Behavioral of spi is
    constant CLK_DIV_RELATION : integer := 100; --SPI CLK = AXI_CLK / CLK_DIV_RELATION 
    constant SPI_BITS : integer := 8;
    constant M_COUNT_BITS : integer := integer(ceil(log2(real(C_S_AXI_DATA_WIDTH))));
    constant DIV_COUNT_BITS : integer := integer(ceil(log2(real(CLK_DIV_RELATION))));
    signal ZEROS   : std_logic_vector(C_S_AXI_DATA_WIDTH-SPI_BITS-1 downto 0) := (others => '0');
    -- FSM
    type state_type is (IDLE,RUN_COMM,INT_OUT); 
    signal state,state_next:state_type;
    signal INT_next:std_logic;
    signal CSn_signal,CSn_next:std_logic;
    -- para espiar los estados
    signal internal_state:STD_LOGIC_VECTOR (1 downto 0);
    -- counters
    signal sclk_count,sclk_count_next:unsigned(DIV_COUNT_BITS downto 0) := (others => '0');
    signal SCLK_signal,SCLK_signal_next:std_logic;
    signal data_count,data_count_next:unsigned(SPI_BITS-1 downto 0);
    -- PISO & SIPO reg
    signal sipo_reg,sipo_next:STD_LOGIC_VECTOR (SPI_BITS-1 downto 0);
    signal piso_reg,piso_next:STD_LOGIC_VECTOR (SPI_BITS -1 downto 0);
    signal S_read,S_load:std_logic;
    -- signal register
    signal RENA_reg,RENA_next:std_logic;
    signal WENA_reg,WENA_next:std_logic;

begin

    --+++++++++++++++++++++++++++++++++++++++++++++++++--
    enable_regs : process( AXI_CLK_I,AXI_RESETn_I,RENA_I,WENA_I )
    begin
        if AXI_RESETn_I='0' then
            RENA_reg <= '0';
            WENA_reg <= '0';
        elsif rising_edge(AXI_CLK_I) then
            RENA_reg <= RENA_next;
            WENA_reg <= WENA_next;  
        end if ;        
    end process ; -- enable_regs
    RENA_next <='1' when RENA_I='1' else
                '0' when state=RUN_COMM else 
                RENA_reg;
    WENA_next <='1' when WENA_I='1' else
                '0' when state=RUN_COMM else 
                WENA_reg;
    --+++++++++++++++++++++++++++++++++++++++++++++++++--

    fsm_registers : process( AXI_CLK_I,AXI_RESETn_I,WADDR_I )
    begin
        if AXI_RESETn_I='0' then
            state <= IDLE;
            INT_O <= '0';
            CSn_signal   <= '1';
        elsif rising_edge(AXI_CLK_I) then
            state      <= state_next;   
            INT_O      <= INT_next;
            CSn_signal <= CSn_next;     
        end if ;        
    end process ; -- fsm_registers
            
    CSn_O  <= CSn_signal;

    next_state : process( state,WADDR_I,RENA_I,SCLK_signal,sclk_count,data_count,RENA_reg,WENA_reg )
    begin
        state_next <= state;
        INT_next <= '0';
        CSn_next   <= '1';
        S_read <= '0';
        S_load <= '0';
        case( state ) is
            when IDLE =>
                internal_state<="00";
                if (WADDR_I=x"0") and (WENA_reg='1') then                    
                    state_next   <= RUN_COMM;
                elsif (RADDR_I=x"4") and (RENA_reg='1') then                    
                    state_next   <= RUN_COMM;
                    S_load <='1';
                end if;
            when RUN_COMM =>
                internal_state<="01";
                S_read <= '1';
                S_load <= '1';
                CSn_next <= '0';
                if data_count = SPI_BITS-1 and sclk_count=CLK_DIV_RELATION-1 then
                    state_next <= INT_OUT;
                    INT_next <= '1'; 
                    CSn_next <= '1';
                    S_read <= '0';
                end if ;
            when INT_OUT =>  
                internal_state<="11";
                INT_next <= '1';
                S_read <= '0';
                S_load <='0';                       
                if sclk_count =CLK_DIV_RELATION-1 then
                    if WENA_reg='1' or RENA_reg='1' then
                        state_next <= RUN_COMM;
                    else
                        state_next <= IDLE;                        
                    end if ;
                    INT_next <= '0';
                end if;
        end case ;        
    end process ; -- next_state

    --+++++++++++++++++++++++++++++++++++++++++++++++++--

    counters : process( AXI_CLK_I,AXI_RESETn_I )
    begin
        if AXI_RESETn_I='0' then
            sclk_count  <= (others => '0');
            data_count  <= (others => '0');
            SCLK_signal <= '0';
        elsif rising_edge(AXI_CLK_I) then
            sclk_count  <= sclk_count_next;
            data_count  <= data_count_next;
            SCLK_signal <= SCLK_signal_next;
        end if;        
    end process ; -- counters

    sclk_count_next  <= (others => '0') when (sclk_count=CLK_DIV_RELATION-1) else 
                        sclk_count+1;              

    SCLK_signal_next <= '0' when sclk_count=CLK_DIV_RELATION-1 or CSn_signal='1' else
                        '1' when sclk_count=(CLK_DIV_RELATION/2)-1 else
                        SCLK_signal;

    SCLK_O <= SCLK_signal;
    
    data_count_next  <= (others => '0') when (data_count=SPI_BITS+1) else
                        data_count+1    when sclk_count=CLK_DIV_RELATION-1   else
                        data_count;    
     

    --+++++++++++++++++++++++++++++++++++++++++++++++++--

     sipo : process( AXI_CLK_I,AXI_RESETn_I,SCLK_signal )
     begin
        if AXI_RESETn_I='0' then
            sipo_reg <= (others =>'0');
        elsif rising_edge(AXI_CLK_I) then
            sipo_reg <= sipo_next;
        end if ;    
     end process ; -- sipo_reg

    sipo_next <= sipo_reg(SPI_BITS-2 downto 0) & MISO_I when S_load='1' and (sclk_count=0) else
                  sipo_reg;

    RDATA_O <= ZEROS & sipo_next when RADDR_I=x"4" else
               WDATA_I when RADDR_I = x"0" else
               (others => '0');

    --+++++++++++++++++++++++++++++++++++++++++++++++++--

     piso : process( AXI_CLK_I,AXI_RESETn_I )
     begin
        if AXI_RESETn_I='0' then
            piso_reg <= (others =>'0');
        elsif rising_edge(AXI_CLK_I) then
            piso_reg <= piso_next;
        end if ;    
     end process ; -- piso_reg

     piso_next <= WDATA_I(SPI_BITS-1 downto 0) when WENA_I='1' else
				  piso_reg when (data_count=0 and SCLK_count<CLK_DIV_RELATION-1) else
                  piso_reg(SPI_BITS-2 downto 0) & '0' when (S_read='1' and SCLK_count=CLK_DIV_RELATION-1) else
                  piso_reg; 

     MOSI_O <= piso_reg(SPI_BITS-1)and S_read when AXI_RESETn_I='1' else '0';

end Behavioral;